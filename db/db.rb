require 'sequel'

class DB

  def db
    @db ||= Sequel.connect('sqlite://db/dev.sqlite3', encoding: 'utf-8')
  end

end
