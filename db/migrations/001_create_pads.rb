
Sequel.migration do
  change do
    create_table :notes do
      primary_key :id
      String :uuid, null: false, index: true
      String :content, text: true, default: ''

      DateTime :created_at
      DateTime :updated_at
    end
  end
end
