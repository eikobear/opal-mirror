require_relative "../views/note_view"

class Notecon < Basecon

  get '/note/:uuid/?' do
    mirror NoteView
  end

end
