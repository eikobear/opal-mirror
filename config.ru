require 'yaml'

require './app'

app_yml_path = "#{Dir.pwd}/config/application.yml"
app_sec_path = "#{Dir.pwd}/config/secrets.yml"

if File.exist? app_yml_path
  CONFIG = YAML.load_file app_yml_path
  ENV.update CONFIG
  puts 'loaded configuration file'
else
  puts 'could not find configuration file. skipping.'
end

if File.exist? app_sec_path
  SECRETS = YAML.load_file app_sec_path
  ENV.update SECRETS
  puts 'loaded secrets file'
else
  puts 'could not find secrets file. skipping.'
end

run App
