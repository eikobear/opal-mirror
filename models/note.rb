require 'securerandom'

class Note < Sequel::Model

  def before_create
    super
    self.uuid = SecureRandom.uuid
  end

end
