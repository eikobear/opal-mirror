require 'sinatra/base'
require './microcon'
require './microcons/basecon'
require './db/db.rb'
#require 'pry-byebug'

class App < Sinatra::Base

  Dir[File.join(__dir__, 'microcons', '*.rb')].each { |file| require file }

  Basecon.descendants.each { |mcon| use mcon }
  
  def self.db
    @@db ||= DB.new.db
  end


end

App.db

Sequel::Model.plugin :timestamps
Dir[File.join(__dir__, 'models', '*.rb')].each { |file| require file }
