require 'pry'


class View

  def self.shards
    @shards ||= {}
  end

  def self.render(name, params)
    self.shards[name].build(params).to_str
  end

  def self.shard(name, &block)
    @tb = TagBlock.new(self, name)
    @tb.block= block
    self.shards[name] = @tb
  end

end

class Tag 

  attr_accessor :inner
  attr_accessor :name
  
  def initialize name
    @name = name
    @classes = []
  end

  def to_str
    content = (inner) ? inner.to_str : ''
    if content.length <= TagBlock::LINE_MAX && !content.include?("\n")
      str = "<#{@name}>#{content}</#{@name}>"
    else
      content.gsub!("\n", "\n  ")
      str = "<#{@name}>\n  #{content}\n</#{@name}>"
    end
    str 
  end

  def class_att class_name
    @classes << class_name
  end

end

class Doctype

  def initialize
  end

  def to_str
    '<!doctype html>'
  end

end

class TagBlock

  LINE_MAX = 30
  attr_accessor :block
  attr_reader :params

  def initialize view, parent
    @view = view
    @parent = parent
    @tags = []
    @content = ''
  end

  def text?
    !@content.empty?
  end

  def count
    @tags.count
  end

  def build params
    @tags = []
    @params = params
    val = instance_eval(&block) if block
    @content = val if val.class == String
    return self
  end

  def p *args
    return self.method_missing(:p, *args, &Proc.new) if block_given?
    self.method_missing(:p, *args)
  end

  def render shard
    @view.render(shard, params)
  end

  def doctype!
    doctype = Doctype.new
    @tags << doctype
    return doctype
  end

  def method_missing name, *args
    tag = if [
      :html, :head, :body, 
      :div, :p, :a, :span, 
      :h1, :h2, :h3, :h4, 
      :h5, :h6, :ul, :ol, 
      :li, :textarea, :title].include?(name)
      Tag.new(name)
    else
      Tag.new(:div).class_att(name)
    end
    if block_given?
      tag.inner = TagBlock.new(@view, name)
      tag.inner.block = Proc.new
      tag.inner.build params
    end
    @tags << tag
    return tag
  end

  def to_str
    if text?
      return @content if @content.length <= LINE_MAX
      chunks = @content.scan(/.{1,#{LINE_MAX}}/).map do |chunk| 
        if chunk[0] == " "
          [""] + chunk.split(" ", -1)
        else
          chunk.split(" ", -1)
        end
      end
      return chunks.inject("") { |rest, chunk| "#{rest}#{chunk[0...-1].join(" ")}\n#{chunk[-1]}" }
    end
    @tags.map { |tag| "#{tag.to_str}" }.join("\n")
  end

end


