require_relative 'view'

class MyView < View

  shard :root do
    doctype!
    html {
      head
      
      body {
      
        h1 { 'welcome to my site!' }

        div {
          
          p { 'at my site we have:' }

          ul {
            li { 'cool stuff' }
            li { 'more cool stuff' }
            li { 'even more cool stuff' }
          }

          p { 'we also have support for REALLY LONG CONTENT. it just wraps around like magic! it even breaks at the nearest space' }
        
        }
      
      }

    }

  end

end
