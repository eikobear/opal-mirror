require_relative 'view'

class NoteView < View

  shard :root do
    doctype!
    html {
      head { title { 'fNote' } }
      
      body {
        render (params['uuid'] == 'test') ? :note : :note_not_found
      }

    }
  end

  shard :note do
    h1 { "note: #{ params['uuid'] }" }
    div { textarea { 'note content goes here' } }
  end

  shard :note_not_found do
    h1 { "note not found! sorry!" }
  end

end
