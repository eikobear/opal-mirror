require 'pry-byebug'
require './db/db.rb'

class App
  
  def self.db
    @@db ||= DB.new.db
  end


end

App.db

Sequel::Model.plugin :timestamps
Dir[File.join(Dir.pwd, 'models', '*.rb')].each { |file| puts "requiring #{file}"; require file }

binding.pry

puts 'quitting console...'
