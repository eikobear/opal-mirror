require 'pry-byebug'

class Microcon < Sinatra::Base

  @@descendants = []

  def self.inherited(subclass)
    @@descendants << subclass
    super
  end

  def self.descendants
    @@descendants
  end

  def mirror view
    puts "params: #{params}"
    view.render :root, params
  end

end
